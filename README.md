loader
======

The idea of this application is to read the values/data of an XML file of products and store them in a database:
- Open the XML file.
- Read/Process the data of the XML file.
- Save the data in the MySQL database (can be one or more data tables)

Requirements:

    - php >= 7.1
    - docker
    - composer (install composer (global or local) https://getcomposer.org/download/)

How to run application:

    1. cd ./compose-files/mysql
    2. docker-compose up --build -d
    3. cd ../../
    4. composer install
    5. php bin/console doctrine:database:drop --force
    6. php bin/console doctrine:database:create
    7. php bin/console doctrine:schema:update --force
    8. php bin/console doctrine:schema:validate
    5. php bin/console load:products <path to XML file with products>

How to run tests:

    run all tests of the application
        ./vendor/bin/simple-phpunit
