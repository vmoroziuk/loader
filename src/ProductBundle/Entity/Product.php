<?php

namespace ProductBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Product
 */
class Product
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $sku;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $weight;

    /**
     * @var bool
     */
    private $isInStock;

    /**
     * @var int
     */
    private $qty;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $description;

    /**
     * @var ProductType
     */
    private $productType;

    /**
     * @var ProductColor
     */
    private $productColor;

    /**
     * @var ProductSize
     */
    private $productSize;

    /**
     * @var Collection|ProductImage[]
     */
    private $productImages;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productImages = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return Product
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return Product
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set isInStock
     *
     * @param boolean $isInStock
     *
     * @return Product
     */
    public function setIsInStock($isInStock)
    {
        $this->isInStock = $isInStock;

        return $this;
    }

    /**
     * Get isInStock
     *
     * @return bool
     */
    public function getIsInStock()
    {
        return $this->isInStock;
    }

    /**
     * Set qty
     *
     * @param integer $qty
     *
     * @return Product
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set productType
     *
     * @param ProductType $productType
     *
     * @return Product
     */
    public function setProductType(ProductType $productType = null)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return ProductType
     */
    public function getProductType()
    {
        return $this->productType;
    }


    /**
     * Set productColor
     *
     * @param ProductColor $productColor
     *
     * @return Product
     */
    public function setProductColor(ProductColor $productColor = null)
    {
        $this->productColor = $productColor;

        return $this;
    }

    /**
     * Get productColor
     *
     * @return ProductColor
     */
    public function getProductColor()
    {
        return $this->productColor;
    }

    /**
     * Set productSize
     *
     * @param ProductSize $productSize
     *
     * @return Product
     */
    public function setProductSize(ProductSize $productSize = null)
    {
        $this->productSize = $productSize;

        return $this;
    }

    /**
     * Get productSize
     *
     * @return ProductSize
     */
    public function getProductSize()
    {
        return $this->productSize;
    }

    /**
     * Add productImage
     *
     * @param ProductImage $productImage
     *
     * @return Product
     */
    public function addProductImage(ProductImage $productImage)
    {
        $this->productImages->add($productImage);
        $productImage->setProduct($this);

        return $this;
    }

    /**
     * Remove productImage
     *
     * @param ProductImage $productImage
     */
    public function removeProductImage(ProductImage $productImage)
    {
        $this->productImages->removeElement($productImage);
    }

    /**
     * Get productImages
     *
     * @return Collection|ProductImage[]
     */
    public function getProductImages()
    {
        return $this->productImages;
    }
}
