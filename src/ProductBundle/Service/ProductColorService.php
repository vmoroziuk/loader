<?php

namespace ProductBundle\Service;


use CommonBundle\Repository\AbstractEntityRepository;
use CommonBundle\Service\AbstractEntityService;
use ProductBundle\Entity\ProductColor;
use ProductBundle\Repository\ProductColorRepository;

class ProductColorService extends AbstractEntityService
{
    const NAME = 'name';

    /**
     * @var ProductColorRepository
     */
    private $productColorRepository;


    /**
     * ProductColorService constructor.
     *
     * @param ProductColorRepository $productColorRepository
     */
    public function __construct(ProductColorRepository $productColorRepository)
    {
        $this->productColorRepository = $productColorRepository;
    }

    /**
     * @return AbstractEntityRepository
     */
    public function getRepository()
    {
        return $this->productColorRepository;
    }

    /**
     * @param string $colorName
     *
     * @return ProductColor
     */
    public function prepareProductColorEntity(string $colorName): ProductColor
    {
        $productColor = $this->productColorRepository->findOneBy([self::NAME => $colorName]);

        if (!$productColor) {
            /** @var ProductColor $productColor */
            $productColor = $this->createNewEntity();

            $productColor->setName($colorName);
        }

        return $productColor;
    }
}
