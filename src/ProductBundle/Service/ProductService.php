<?php

namespace ProductBundle\Service;

use CommonBundle\Repository\AbstractEntityRepository;
use CommonBundle\Service\AbstractEntityService;
use ProductBundle\Entity\Product;
use ProductBundle\Entity\ProductImage;
use ProductBundle\Repository\ProductRepository;
use ProductBundle\Service\Exception\ProductLoadingException;
use SimpleXMLElement;
use Exception;

class ProductService extends AbstractEntityService
{
    const IN_STOCK = 'in stock';
    const PRODUCT_CONTAINS_LOCAL_NAME_IMAGE = '//product/*[contains(local-name(), \'image\')]';
    const CAN_T_LOAD_PRODUCT_ERROR_MESSAGE = "Can't load product: %s";

    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ProductSizeService
     */
    private $productSizeService;
    /**
     * @var ProductColorService
     */
    private $productColorService;
    /**
     * @var ProductImageService
     */
    private $productImageService;
    /**
     * @var ProductTypeService
     */
    private $productTypeService;


    /**
     * ProductService constructor.
     *
     * @param ProductRepository   $productRepository
     * @param ProductSizeService  $productSizeService
     * @param ProductColorService $productColorService
     * @param ProductImageService $productImageService
     * @param ProductTypeService  $productTypeService
     */
    public function __construct(
        ProductRepository $productRepository,
        ProductSizeService $productSizeService,
        ProductColorService $productColorService,
        ProductImageService $productImageService,
        ProductTypeService $productTypeService
    ) {
        $this->productRepository        = $productRepository;
        $this->productSizeService       = $productSizeService;
        $this->productColorService      = $productColorService;
        $this->productImageService      = $productImageService;
        $this->productTypeService       = $productTypeService;
    }

    /**
     * @return AbstractEntityRepository
     */
    public function getRepository()
    {
        return $this->productRepository;
    }

    /**
     * @param SimpleXMLElement $productXmlItemObject
     *
     * @throws ProductLoadingException
     */
    public function saveProductEntity(SimpleXMLElement $productXmlItemObject): void {
        try {
            /** @var Product $product */
            $product = $this::createNewEntity();

            $product->setName($productXmlItemObject->name);
            $product->setProductType($this->productTypeService->prepareProductTypeEntity($productXmlItemObject->type));
            $product->setProductSize($this->productSizeService->prepareProductSizeEntity($productXmlItemObject->size));
            $product->setProductColor(
                $this->productColorService->prepareProductColorEntity($productXmlItemObject->color));
            $product->setSku($productXmlItemObject->sku);
            $product->setWeight(floatval($productXmlItemObject->weight));
            $product->setIsInStock($this->isProductInStock($productXmlItemObject));
            $product->setQty(intval($productXmlItemObject->qty));
            $product->setPrice(floatval($productXmlItemObject->price));
            $product->setDescription($productXmlItemObject->description);

            $this->addImages($productXmlItemObject, $product);

            $this->productRepository->save($product);
        } catch (Exception $e) {
            throw new ProductLoadingException(printf(
                self::CAN_T_LOAD_PRODUCT_ERROR_MESSAGE, $productXmlItemObject->name), $e->getCode(), $e->getPrevious());
        }
    }

    /**
     * @param SimpleXMLElement $productXmlItemObject
     *
     * @return bool
     */
    private function isProductInStock(SimpleXMLElement $productXmlItemObject): bool {
        return $productXmlItemObject->is_in_stock && $productXmlItemObject->is_in_stock === self::IN_STOCK;
    }

    /**
     * @param SimpleXMLElement $productXmlItemObject
     * @param Product          $product
     */
    private function addImages(SimpleXMLElement $productXmlItemObject, Product $product): void {
        $simpleXMLElements = $productXmlItemObject->xpath(self::PRODUCT_CONTAINS_LOCAL_NAME_IMAGE);

        foreach ($simpleXMLElements as $url) {
            /** @var ProductImage $productImageEntity */
            $productImageEntity = $this->productImageService->createNewEntity();
            $productImageEntity->setImageUrl($url);
            $product->addProductImage($productImageEntity);
        }
    }
}
