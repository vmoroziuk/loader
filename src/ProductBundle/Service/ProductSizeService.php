<?php

namespace ProductBundle\Service;


use CommonBundle\Repository\AbstractEntityRepository;
use CommonBundle\Service\AbstractEntityService;
use ProductBundle\Entity\ProductSize;
use ProductBundle\Repository\ProductSizeRepository;

class ProductSizeService extends AbstractEntityService
{
    const NAME = 'name';

    /**
     * @var ProductSizeRepository
     */
    private $productSizeRepository;


    /**
     * ProductSizeService constructor.
     *
     * @param ProductSizeRepository $productSizeRepository
     */
    public function __construct(ProductSizeRepository $productSizeRepository)
    {
        $this->productSizeRepository = $productSizeRepository;
    }

    /**
     * @return AbstractEntityRepository
     */
    public function getRepository()
    {
        return $this->productSizeRepository;
    }

    /**
     * @param string $sizeName
     *
     * @return ProductSize
     */
    public function prepareProductSizeEntity(string $sizeName): ProductSize
    {
        $productSize = $this->productSizeRepository->findOneBy([self::NAME => $sizeName]);

        if (!$productSize) {
            /** @var ProductSize $productSize */
            $productSize = $this->createNewEntity();

            $productSize->setName($sizeName);
        }

        return $productSize;
    }
}
