<?php

namespace ProductBundle\Service\Exception;

use Exception;

class ProductLoadingException extends Exception
{
}
