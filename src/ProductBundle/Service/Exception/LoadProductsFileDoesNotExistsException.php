<?php

namespace ProductBundle\Service\Exception;

use RuntimeException;

class LoadProductsFileDoesNotExistsException extends RuntimeException
{
}
