<?php
/**
 * Created by PhpStorm.
 * User: yakut
 * Date: 9/13/18
 * Time: 10:54 PM
 */

namespace ProductBundle\Service;


use CommonBundle\Repository\AbstractEntityRepository;
use CommonBundle\Service\AbstractEntityService;
use ProductBundle\Repository\ProductImageRepository;

class ProductImageService extends AbstractEntityService
{
    /**
     * @var ProductImageRepository
     */
    private $productImageRepository;


    /**
     * ProductImageService constructor.
     *
     * @param ProductImageRepository $productImageRepository
     */
    public function __construct(ProductImageRepository $productImageRepository)
    {
        $this->productImageRepository = $productImageRepository;
    }

    /**
     * @return AbstractEntityRepository
     */
    public function getRepository()
    {
        return $this->productImageRepository;
    }
}
