<?php

namespace ProductBundle\Service;

use Doctrine\ORM\EntityManager;
use ProductBundle\Entity\ProductType;
use ProductBundle\Repository\ProductTypeRepository;

class ProductTypeService
{
    const DELIMITER = ' > ';
    const TITLE = 'title';

    /**
     * @var ProductTypeRepository
     */
    private $productTypeRepository;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * ProductTypeService constructor.
     *
     * @param ProductTypeRepository $productTypeRepository
     *
     * @internal param LoggerInterface $logger
     */
    public function __construct(
        ProductTypeRepository $productTypeRepository
    ) {
        $this->productTypeRepository = $productTypeRepository;
        $this->em = $this->productTypeRepository->getEntityManager();
    }

    /**
     * @param string $typePath
     *
     * @return ProductType
     */
    public function prepareProductTypeEntity(string $typePath): ProductType
    {
        $arrayPath = explode(self::DELIMITER, $typePath);
        /** @var ProductType $parentProductType */
        $parentProductType = NULL;

        foreach ($arrayPath as $item) {
            $productType = $this->productTypeRepository->findOneBy([self::TITLE => $item]);

            if (!$productType) {
                $newProductType = new ProductType();
                $newProductType->setTitle($item);

                if ($parentProductType) {
                    $newProductType->setParent($parentProductType);
                }

                $this->em->persist($newProductType);
                $parentProductType = $newProductType;
            } else {
                $parentProductType = $productType;
            }
        }

        return $parentProductType;
    }
}
