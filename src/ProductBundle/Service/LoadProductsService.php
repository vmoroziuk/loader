<?php

namespace ProductBundle\Service;

use CommonBundle\Service\HybridXMLParserService;
use ProductBundle\Service\Exception\LoadProductsFileDoesNotExistsException;
use ProductBundle\Service\Exception\ProductLoadingException;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;


class LoadProductsService implements LoadProductsInterface
{
    const FILE_DOES_NOT_EXISTS_ERROR_MESSAGE = 'File does not exists';
    const ERROR_PRODUCT_ITEM_STRUCTURE_ERROR_MESSAGE = "Error product item structure, field: %s\r\n";
    const PRODUCT_ITEM_WITH_INCORRECT_STRUCTURE_ERROR_MESSAGE = "Product item with incorrect structure, sku: %s\r\n";
    const AN_EXCEPTION_OCCURRED_WHILE_LOADING_THE_PRODUCT_ERROR_MESSAGE =
        "An exception occurred while loading the product: ";
    const PATH = '/rss/channel/product';
    const PRODUCT_STRUCTURE = [
        'name',
        'type',
        'size',
        'color',
        'sku',
        'weight',
        'is_in_stock',
        'qty',
        'price',
        'description',
    ];

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var HybridXMLParserService
     */
    private $hybridXMLParser;
    /**
     * @var ProductService
     */
    private $productService;


    /**
     * LoadProductsService constructor.
     *
     * @param LoggerInterface        $logger
     * @param HybridXMLParserService $hybridXMLParser
     * @param ProductService         $productService
     */
    public function __construct(
        LoggerInterface $logger,
        HybridXMLParserService $hybridXMLParser,
        ProductService $productService
    ) {
        $this->logger           = $logger;
        $this->hybridXMLParser  = $hybridXMLParser;
        $this->productService   = $productService;
    }

    /**
     * @param string $path
     *
     * @return void
     * @throws LoadProductsFileDoesNotExistsException
     */
    public function loadFromXmlFile(string $path): void
    {
        $this->logger->info('Start Products loading...');

        if (!file_exists($path)) {
            $this->logger->error(self::FILE_DOES_NOT_EXISTS_ERROR_MESSAGE);
            throw new LoadProductsFileDoesNotExistsException(self::FILE_DOES_NOT_EXISTS_ERROR_MESSAGE);
        }

        ini_set('memory_limit', '2048M');

        $this->processFile($path);

        $this->logger->info('Done.');
    }

    /**
     * @param string $path
     */
    private function processFile(string $path): void
    {
        $this->hybridXMLParser
            ->bind(self::PATH, function (SimpleXMLElement $productXmlItemObject) {
                $this->processProductsLoading($productXmlItemObject);
            })
            ->process($path);
    }

    /**
     * @param SimpleXMLElement $productXmlItemObject
     */
    private function processProductsLoading(SimpleXMLElement $productXmlItemObject): void {
        if ($this->validateProductStructure($productXmlItemObject)) {
            try {
                $this->productService->saveProductEntity($productXmlItemObject);
            } catch (ProductLoadingException $e) {
                $this->logger->error(self::AN_EXCEPTION_OCCURRED_WHILE_LOADING_THE_PRODUCT_ERROR_MESSAGE, $e);
            }
        } else {
            $this->logger->error(printf(
                self::PRODUCT_ITEM_WITH_INCORRECT_STRUCTURE_ERROR_MESSAGE, $productXmlItemObject->sku));
        }
    }

    /**
     * @param SimpleXMLElement $productXmlItemObject
     *
     * @return bool
     */
    private function validateProductStructure(SimpleXMLElement $productXmlItemObject): bool {
        $result = true;
        foreach (self::PRODUCT_STRUCTURE as $key => $value) {
            if (!property_exists($productXmlItemObject, $value)) {
                $this->logger->error(printf(self::ERROR_PRODUCT_ITEM_STRUCTURE_ERROR_MESSAGE, $value));
                $result = false;
                break;
            }
        }

        return $result;
    }
}
