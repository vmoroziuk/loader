<?php

namespace ProductBundle\Service;


interface LoadProductsInterface
{
    /**
     * @param string $path
     *
     * @return mixed
     */
    public function loadFromXmlFile(string $path): void;
}
