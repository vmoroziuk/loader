<?php

namespace ProductBundle\Repository;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use ProductBundle\Entity\ProductType;

/**
 * ProductTypeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductTypeRepository extends NestedTreeRepository
{
    /**
     * ProductTypeRepository constructor.
     *
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $entityClass = ProductType::class;

        /** @var EntityManagerInterface $manager */
        $manager = $managerRegistry->getManagerForClass($entityClass);
        /** @var ClassMetadata $classMetadata */
        $classMetadata = $manager->getClassMetadata($entityClass);

        parent::__construct($manager, $classMetadata);
    }

    public function getEntityManager()
    {
        return parent::getEntityManager();
    }
}
