<?php

namespace CommonBundle\Service;

use CommonBundle\Service\Exception\HybridXMLParserException;
use InvalidArgumentException;
use XMLReader;

/**
 * Hybrid XML parser
 *
 * Class to parse huge XML files in a memory-efficient way.
 *
 * @author  Alexander Kovalenko <alexanderk23@gmail.com>
 * @link    https://github.com/alexanderk23/hybrid-xml-parser
 * @license Public Domain
 */
class HybridXMLParserService
{
    const ANOTHER_LISTENER_IS_ALREADY_BOUND_TO_PATH = 'Another listener is already bound to path ';
    const CANNOT_OPEN = 'Cannot open ';
    const LISTENER_IS_NOT_CALLABLE = 'Listener is not callable';

    protected $xml;
    protected $uri;
    protected $path;
    protected $stop;
    protected $pathListeners = [];
    protected $ignoreCase = false;

    /**
     * @param boolean $ignoreCase Ignore XML tags case
     */
    public function __construct($ignoreCase = true)
    {
        $this->xml        = new XMLReader;
        $this->ignoreCase = $ignoreCase;
    }

    protected function convertCase($s)
    {
        return $this->ignoreCase ? mb_strtolower($s) : $s;
    }

    /**
     * @param string $path XML path to watch for (slash-separated)
     * @param mixed  $listener Callable (lambda, inline function, function name as string, or array(class, method))
     *
     * @return HybridXMLParserService
     * @throws HybridXMLParserException
     */
    public function bind($path, $listener)
    {
        if (!is_callable($listener)) {
            throw new InvalidArgumentException(self::LISTENER_IS_NOT_CALLABLE);
        }
        $path = $this->convertCase($path);
        if (isset($this->pathListeners[$path])) {
            throw new HybridXMLParserException(self::ANOTHER_LISTENER_IS_ALREADY_BOUND_TO_PATH . $path);
        }
        $this->pathListeners[$path] = $listener;

        return $this;
    }

    public function unbind($path)
    {
        $path = $this->convertCase($path);
        if (isset($this->pathListeners[$path])) {
            unset($this->pathListeners[$path]);
        }

        return $this;
    }

    public function unbindAll()
    {
        $this->pathListeners = [];

        return $this;
    }

    public function stop()
    {
        $this->stop = true;

        return $this;
    }

    protected function getCurrentPath()
    {
        return '/' . join('/', $this->path);
    }

    protected function notifyListener($path)
    {
        if (isset($this->pathListeners[$path])) {
            $this->pathListeners[$path](
                new \SimpleXMLElement($this->xml->readOuterXML(), LIBXML_COMPACT | LIBXML_NOCDATA),
                $this
            );
        }

        return $this;
    }

    public function process($uri, $options = 0)
    {
        $this->path = [];

        if (!$this->xml->open($uri, null, $options | LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_PARSEHUGE)) {
            throw new HybridXMLParserException(self::CANNOT_OPEN . $uri);
        }

        $this->stop = false;

        while (!$this->stop && $this->xml->read()) {
            switch ($this->xml->nodeType) {
                case XMLReader::ELEMENT:
                    array_push($this->path, $this->convertCase($this->xml->name));
                    $path = $this->getCurrentPath();
                    $this->notifyListener($path);
                    if (!$this->xml->isEmptyElement) {
                        break;
                    }
                case XMLReader::END_ELEMENT:
                    array_pop($this->path);
                    break;
            }
        }

        $this->xml->close();

        return $this;
    }
}
