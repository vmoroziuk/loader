<?php

namespace CommonBundle\Service;

use CommonBundle\Repository\AbstractEntityRepository;
use Doctrine\ORM\EntityManager;

abstract class AbstractEntityService
{
    /**
     * @return AbstractEntityRepository
     */
    abstract public function getRepository();

    /**
     * @return object
     */
    public function createNewEntity()
    {
        /** @var AbstractEntityRepository $repository */
        $repository = $this->getRepository();
        $entityClass = $repository->getEntityClass();

        return new $entityClass;
    }

    /**
     * Find entity by id without any relations
     *
     * @param $id
     * @return null|object
     */
    public function get($id)
    {
        /** @var AbstractEntityRepository $repository */
        $repository = $this->getRepository();

        return $repository->find($id);
    }

    /**
     * @param int $entityId
     * @return bool|\Doctrine\Common\Proxy\Proxy|null|object
     * @throws \Doctrine\ORM\ORMException
     */
    public function getReference($entityId)
    {
        /** @var AbstractEntityRepository $repository */
        $repository = $this->getRepository();

        /** @var EntityManager $em */
        $em = $repository->getEm();

        return $em->getReference($repository->getClassName(), $entityId);
    }
}
