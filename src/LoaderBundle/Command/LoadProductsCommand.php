<?php

namespace LoaderBundle\Command;

use ProductBundle\Service\LoadProductsInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadProductsCommand extends ContainerAwareCommand
{
    const LOAD_PRODUCTS = 'load:products';
    const LOAD_PRODUCTS_FROM_XML_FILE = 'Load products from xml file';
    const THE_PATH_TO_THE_XML_FILE_WITH_PRODUCTS_DATA = 'The path to the XML file with products data';

    /**
     * @var LoadProductsInterface
     */
    private $loaderProductsService;


    /**
     * LoadProductsCommand constructor.
     *
     * @param LoadProductsInterface $loadProductsService
     */
    public function __construct(LoadProductsInterface $loadProductsService)
    {
        parent::__construct();

        $this->loaderProductsService = $loadProductsService;
    }

    protected function configure()
    {
        $this
            ->setName(self::LOAD_PRODUCTS)
            ->setDescription(self::LOAD_PRODUCTS_FROM_XML_FILE)
            ->addArgument('pathToXmlFile', InputArgument::REQUIRED, self::THE_PATH_TO_THE_XML_FILE_WITH_PRODUCTS_DATA);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('pathToXmlFile');

        $this->loaderProductsService->loadFromXmlFile($path);
    }

}
