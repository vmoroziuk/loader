<?php

namespace tests\ProductBundle\Entity;


use PHPUnit\Framework\TestCase;
use ProductBundle\Entity\Product;
use ProductBundle\Entity\ProductColor;
use ProductBundle\Entity\ProductImage;
use ProductBundle\Entity\ProductSize;
use ProductBundle\Entity\ProductType;

class ProductUnitTest extends TestCase
{
    public function testSetNameAndGetName()
    {
        $product = new Product();
        $name = 'Test';
        $product->setName($name);

        $this->assertEquals($product->getName(), $name, 'Set and get names are not equal');
    }

    public function testSetSkuAndGetSku()
    {
        $product = new Product();
        $sku = 'Test';
        $product->setSku($sku);

        $this->assertEquals($product->getSku(), $sku, 'Set and get sku are not equal');
    }

    public function testSetWeightAndGetWeight()
    {
        $product = new Product();
        $weight = 12;
        $product->setWeight($weight);

        $this->assertEquals($product->getWeight(), $weight, 'Set and get weights are not equal');
    }

    public function testSetIsInStockAndGetIsInStock()
    {
        $product = new Product();
        $isInStock = true;
        $product->setIsInStock($isInStock);

        $this->assertEquals($product->getIsInStock(), $isInStock, 'Set and get isInStocks are not equal');
    }

    public function testSetQtyAndGetQty()
    {
        $product = new Product();
        $qty = 1;
        $product->setQty($qty);

        $this->assertEquals($product->getQty(), $qty, 'Set and get qty are not equal');
    }

    public function testSetPriceAndGetPrice()
    {
        $product = new Product();
        $price = 1.1;
        $product->setPrice($price);

        $this->assertEquals($product->getPrice(), $price, 'Set and get prices are not equal');
    }

    public function testSetDescriptionAndGetDescription()
    {
        $product = new Product();
        $description = "Test";
        $product->setDescription($description);

        $this->assertEquals($product->getDescription(), $description, 'Set and get descriptions are not equal');
    }

    public function testSetProductTypeAndGetProductType()
    {
        $productType = new ProductType();

        $product = new Product();
        $product->setProductType($productType);

        $this->assertEquals($product->getProductType(), $productType, 'Set and get product types are not equal');
    }

    public function testSetProductColorAndGetProductColor()
    {
        $productColor = new ProductColor();

        $product = new Product();
        $product->setProductColor($productColor);

        $this->assertEquals($product->getProductColor(), $productColor, 'Set and get product colors are not equal');
    }

    public function testSetProductSizeAndGetProductSize()
    {
        $productSize = new ProductSize();

        $product = new Product();
        $product->setProductSize($productSize);

        $this->assertEquals($product->getProductSize(), $productSize, 'Set and get product sizes are not equal');
    }

    public function testSetProductImageAndGetProductSize()
    {
        $productImage = new ProductImage();
        $productImage->setImageUrl("test url");

        $product = new Product();
        $product->addProductImage($productImage);

        $this->assertEquals($product->getProductImages()[0], $productImage, 'Set and get product images are not equal');
    }
}
