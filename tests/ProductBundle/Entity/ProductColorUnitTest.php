<?php

namespace tests\ProductBundle\Entity;

use Doctrine\Bundle\DoctrineCacheBundle\Tests\TestCase;
use ProductBundle\Entity\ProductColor;

class ProductColorUnitTest extends TestCase
{
    public function testSetNameAndGetName()
    {
        $productColor = new ProductColor();
        $name = 'Test Color';
        $productColor->setName($name);

        $this->assertEquals($productColor->getName(), $name, 'Set and get names are not equal');
    }
}
