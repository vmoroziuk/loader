<?php

namespace tests\ProductBundle\Entity;

use Doctrine\Bundle\DoctrineCacheBundle\Tests\TestCase;
use ProductBundle\Entity\ProductSize;

class ProductSizeUnitTest extends TestCase
{
    public function testSetNameAndGetName()
    {
        $productSize = new ProductSize();
        $name = 'Test Size';
        $productSize->setName($name);

        $this->assertEquals($productSize->getName(), $name, 'Set and get names are not equal');
    }
}
