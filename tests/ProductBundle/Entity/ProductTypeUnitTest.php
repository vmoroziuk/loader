<?php

namespace tests\ProductBundle\Entity;

use Doctrine\Bundle\DoctrineCacheBundle\Tests\TestCase;
use ProductBundle\Entity\ProductType;

class ProductTypeUnitTest extends TestCase
{
    public function testSetTitleAndGetTitle()
    {
        $productType = new ProductType();
        $title = 'Test type';
        $productType->setTitle($title);

        $this->assertEquals($productType->getTitle(), $title, 'Set and get titles are not equal');
    }

    public function testSetParentAndGetParent()
    {
        $productTypeParent = new ProductType();

        $productType = new ProductType();
        $productType->setParent($productTypeParent);

        $this->assertEquals($productType->getParent(), $productTypeParent, 'Set and get parents are not equal');
    }
}
