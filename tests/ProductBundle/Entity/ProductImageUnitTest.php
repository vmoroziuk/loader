<?php

namespace tests\ProductBundle\Entity;

use Doctrine\Bundle\DoctrineCacheBundle\Tests\TestCase;
use ProductBundle\Entity\ProductImage;

class ProductImageUnitTest extends TestCase
{
    public function testSetImageUrlAndGetImageUrl()
    {
        $productImage = new ProductImage();
        $imageUrl = 'Test url';
        $productImage->setImageUrl($imageUrl);

        $this->assertEquals($productImage->getImageUrl(), $imageUrl, 'Set and get ImageUrls are not equal');
    }
}
