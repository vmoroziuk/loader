<?php

namespace tests\ProductBundle\Service;


use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ProductBundle\Repository\ProductImageRepository;
use ProductBundle\Service\ProductImageService;

class ProductImageServiceUnitTest extends TestCase
{
    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $productImageRepositoryMock;
    /** @var  ProductImageService */
    private $productImageService;

    public function setUp()
    {
        parent::setUp();

        /** @var ProductImageRepository productImageRepositoryMock */
        $this->productImageRepositoryMock = $this->createMock(ProductImageRepository::class);
        $this->productImageService        = new ProductImageService($this->productImageRepositoryMock);
    }

    public function testGetRepository() {
        $this->assertEquals($this->productImageService->getRepository(), $this->productImageRepositoryMock);
    }
}
