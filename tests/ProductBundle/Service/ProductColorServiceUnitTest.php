<?php

namespace tests\ProductBundle\Service;


use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ProductBundle\Entity\ProductColor;
use ProductBundle\Repository\ProductColorRepository;
use ProductBundle\Service\ProductColorService;

class ProductColorServiceUnitTest extends TestCase
{
    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $productColorRepositoryMock;
    /** @var  ProductColorService */
    private $productColorService;

    public function setUp()
    {
        parent::setUp();

        /** @var ProductColorRepository productColorRepositoryMock */
        $this->productColorRepositoryMock = $this->createMock(ProductColorRepository::class);
        $this->productColorService        = new ProductColorService($this->productColorRepositoryMock);
    }

    public function testGetRepository() {
        $this->assertEquals($this->productColorService->getRepository(), $this->productColorRepositoryMock);
    }

    public function testPrepareProductColorEntityIsPresentInDb() {
        $colorName = 'Test';
        $expectedEntity = new ProductColor();
        $expectedEntity->setName($colorName);

        $this->productColorRepositoryMock->expects($this->any())
            ->method('findOneBy')
            ->willReturn($expectedEntity);

        $actualEntity = $this->productColorService->prepareProductColorEntity($colorName);

        $this->assertEquals($expectedEntity, $actualEntity);
    }

    public function testPrepareProductColorEntityIsNotPresentInDb() {
        $colorName = 'Test';
        $expectedEntity = new ProductColor();
        $expectedEntity->setName($colorName);

        $this->productColorRepositoryMock->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $this->productColorRepositoryMock->method('getEntityClass')->willReturn(ProductColor::class);

        $actualEntity = $this->productColorService->prepareProductColorEntity($colorName);

        $this->assertEquals($expectedEntity, $actualEntity);
    }
}
