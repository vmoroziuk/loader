<?php

namespace tests\ProductBundle\Service;


use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ProductBundle\Entity\ProductType;
use ProductBundle\Repository\ProductTypeRepository;
use ProductBundle\Service\ProductTypeService;

class ProductTypeServiceUnitTest extends TestCase
{
    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $productTypeRepositoryMock;
    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $entityManagerMock;
    /** @var  ProductTypeService */
    private $productTypeService;

    public function setUp()
    {
        parent::setUp();

        /** @var ProductTypeRepository productTypeRepositoryMock */
        $this->productTypeRepositoryMock  = $this->createMock(ProductTypeRepository::class);
        /** @var EntityManager entityManagerMock */
        $this->entityManagerMock          = $this->createMock(EntityManager::class);

        $this->productTypeRepositoryMock->expects($this->once())
            ->method('getEntityManager')
            ->willReturn($this->entityManagerMock);

        $this->productTypeService         = new ProductTypeService($this->productTypeRepositoryMock);
    }

    public function testPrepareProductTypeEntityIsPresentInDb() {
        $title = 'Test';
        $expectedEntity = new ProductType();
        $expectedEntity->setTitle($title);

        $this->productTypeRepositoryMock->expects($this->any())
            ->method('findOneBy')
            ->willReturn($expectedEntity);

        $actualEntity = $this->productTypeService->prepareProductTypeEntity($title);

        $this->assertEquals($expectedEntity, $actualEntity);
    }

    public function testPrepareProductTypeEntityIsNotPresentInDb() {
        $titlePath = 'Test1 > Test2';

        $productType1 = new ProductType();
        $productType1->setTitle('Test1');

        $expectedEntity = new ProductType();
        $expectedEntity->setTitle('Test2');
        $expectedEntity->setParent($productType1);

        $this->entityManagerMock->expects($this->any())
            ->method('persist')
            ->willReturn(null);

        $this->productTypeRepositoryMock->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $actualEntity = $this->productTypeService->prepareProductTypeEntity($titlePath);

        $this->assertEquals($expectedEntity, $actualEntity);
    }
}
