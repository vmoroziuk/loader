<?php

namespace tests\ProductBundle\Service;


use Exception;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ProductBundle\Entity\Product;
use ProductBundle\Entity\ProductColor;
use ProductBundle\Entity\ProductImage;
use ProductBundle\Entity\ProductSize;
use ProductBundle\Entity\ProductType;
use ProductBundle\Repository\ProductRepository;
use ProductBundle\Service\Exception\ProductLoadingException;
use ProductBundle\Service\ProductColorService;
use ProductBundle\Service\ProductImageService;
use ProductBundle\Service\ProductService;
use ProductBundle\Service\ProductSizeService;
use ProductBundle\Service\ProductTypeService;
use SimpleXMLElement;

class ProductServiceUnitTest extends TestCase
{
    const PATH = '/rss/channel/product';

    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $productRepositoryMock;
    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $productSizeServiceMock;
    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $productColorServiceMock;
    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $productImageServiceMock;
    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $productTypeServiceMock;
    /** @var  ProductService */
    private $productService;

    public function setUp()
    {
        parent::setUp();

        /** @var ProductRepository productRepositoryMock */
        $this->productRepositoryMock    = $this->createMock(ProductRepository::class);

        $this->productSizeServiceMock   = $this->createMock(ProductSizeService::class);
        $this->productColorServiceMock  = $this->createMock(ProductColorService::class);
        $this->productImageServiceMock  = $this->createMock(ProductImageService::class);
        $this->productTypeServiceMock   = $this->createMock(ProductTypeService::class);
        $this->productService           = new ProductService($this->productRepositoryMock,
            $this->productSizeServiceMock,
            $this->productColorServiceMock,
            $this->productImageServiceMock,
            $this->productTypeServiceMock
        );

    }

    public function testGetRepository() {
        $this->assertEquals($this->productService->getRepository(), $this->productRepositoryMock);
    }

    public function testSaveProductEntityWithSuccessResult() {
        $tmpXmlFile = new SimpleXMLElement(file_get_contents(__DIR__ . '/Resources/testfile1.xml'));
        /** @var SimpleXMLElement $productXmlItemObject */
        $productXmlItemObject = $tmpXmlFile->xpath(self::PATH)[0];

        $productType = new ProductType();
        $productType->setTitle('Evening Dresses');

        $productSize = new ProductSize();
        $productSize->setName('Large - UK (12-14)');

        $productColor = new ProductColor();
        $productColor->setName('Teal');

        $productImage = new ProductImage();
        $productImage->setImageUrl('http://fashiondropshippers.com/media/catalog/product/i/m/image_458.jpg');

        $this->productRepositoryMock->expects($this->any())
            ->method('getEntityClass')
            ->willReturn(Product::class);

        $this->productTypeServiceMock->expects($this->any())
            ->method('prepareProductTypeEntity')
            ->willReturn($productType);

        $this->productSizeServiceMock->expects($this->any())
            ->method('prepareProductSizeEntity')
            ->willReturn($productSize);

        $this->productColorServiceMock->expects($this->any())
            ->method('prepareProductColorEntity')
            ->willReturn($productColor);

        $this->productImageServiceMock->expects($this->any())
            ->method('createNewEntity')
            ->willReturn($productImage);

        $this->productRepositoryMock->expects($this->at(1))
            ->method('save')
            ->willReturn(null);

        $this->productService->saveProductEntity($productXmlItemObject);
    }

    /**
     *
     */
    public function testSaveProductEntityWithException() {
        $tmpXmlFile = new SimpleXMLElement(file_get_contents(__DIR__ . '/Resources/testfile1.xml'));
        /** @var SimpleXMLElement $productXmlItemObject */
        $productXmlItemObject = $tmpXmlFile->xpath(self::PATH)[0];

        $productType = new ProductType();
        $productType->setTitle('Evening Dresses');

        $productSize = new ProductSize();
        $productSize->setName('Large - UK (12-14)');

        $productColor = new ProductColor();
        $productColor->setName('Teal');

        $productImage = new ProductImage();
        $productImage->setImageUrl('http://fashiondropshippers.com/media/catalog/product/i/m/image_458.jpg');

        $this->productRepositoryMock->expects($this->any())
            ->method('getEntityClass')
            ->willReturn(Product::class);

        $this->productTypeServiceMock->expects($this->any())
            ->method('prepareProductTypeEntity')
            ->willReturn($productType);

        $this->productSizeServiceMock->expects($this->any())
            ->method('prepareProductSizeEntity')
            ->willReturn($productSize);

        $this->productColorServiceMock->expects($this->any())
            ->method('prepareProductColorEntity')
            ->willReturn($productColor);

        $this->productImageServiceMock->expects($this->any())
            ->method('createNewEntity')
            ->willReturn($productImage);

        $exception = new Exception();
        $this->productRepositoryMock->expects($this->any())
            ->method('save')
            ->willThrowException($exception);

        $this->expectException(ProductLoadingException::class);

        $this->productService->saveProductEntity($productXmlItemObject);
    }
}
