<?php

namespace tests\ProductBundle\Service;


use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use ProductBundle\Entity\ProductSize;
use ProductBundle\Repository\ProductSizeRepository;
use ProductBundle\Service\ProductSizeService;

class ProductSizeServiceUnitTest extends TestCase
{
    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $productSizeRepositoryMock;
    /** @var  ProductSizeService */
    private $productSizeService;

    public function setUp()
    {
        parent::setUp();

        /** @var ProductSizeRepository productSizeRepositoryMock */
        $this->productSizeRepositoryMock = $this->createMock(ProductSizeRepository::class);
        $this->productSizeService        = new ProductSizeService($this->productSizeRepositoryMock);
    }

    public function testGetRepository() {
        $this->assertEquals($this->productSizeService->getRepository(), $this->productSizeRepositoryMock);
    }

    public function testPrepareProductSizeEntityIsPresentInDb() {
        $sizeName = 'Test';
        $expectedEntity = new ProductSize();
        $expectedEntity->setName($sizeName);

        $this->productSizeRepositoryMock->expects($this->any())
            ->method('findOneBy')
            ->willReturn($expectedEntity);

        $actualEntity = $this->productSizeService->prepareProductSizeEntity($sizeName);

        $this->assertEquals($expectedEntity, $actualEntity);
    }

    public function testPrepareProductSizeEntityIsNotPresentInDb() {
        $sizeName = 'Test';
        $expectedEntity = new ProductSize();
        $expectedEntity->setName($sizeName);

        $this->productSizeRepositoryMock->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $this->productSizeRepositoryMock->method('getEntityClass')->willReturn(ProductSize::class);

        $actualEntity = $this->productSizeService->prepareProductSizeEntity($sizeName);

        $this->assertEquals($expectedEntity, $actualEntity);
    }
}
